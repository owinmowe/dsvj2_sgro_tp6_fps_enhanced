﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{

    [Header("Scene Related")]
    [SerializeField] Scene mainMenuScene;
    [SerializeField] Scene ingameScene;
    [SerializeField] Scene endScene;

    [Header("Score Related")]
    int points = 0;
    Player player;

    static GameManager instance;

    public static GameManager Get() { return instance; }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void LoadIngame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(ingameScene.handle);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void LoadMainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(mainMenuScene.handle);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void LoadEndScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(endScene.handle);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void ExitGame()
    {
        Debug.Log("Exit Game");
        Application.Quit();
    }

    public void SetPoints(int p)
    {
        points = p;
    }

    public int GetPoints()
    {
        return points;
    }

    public void SetCurrentPlayerReference(Player p)
    {
        player = p;
    }

    public Player getCurrentPlayerReference()
    {
        return player;
    }

}
